package com.luongtx.jsontotree;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JsonToTreeApplication {

    public static void main(String[] args) {
        SpringApplication.run(JsonToTreeApplication.class, args);
    }
}
