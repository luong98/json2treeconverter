package com.luongtx.jsontotree;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class JsonToTreeController {
    @PostMapping(value = "/api/convert")
    public ResponseEntity<String> convert(@RequestBody String json, @RequestParam(name = "rootName", defaultValue = "Root") String rootName) {
        JsonToClassGenerator.generateClasses(rootName, json);
        String result = ClassToTreeConverter.convertToTree(rootName);
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }
}
