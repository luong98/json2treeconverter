import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.luongtx.jsontotree.ClassToTreeConverter;
import com.luongtx.jsontotree.JsonToClassGenerator;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;


public class TreeConverterTest {
    public static void main(String[] args) {

    }

    @Test
    void test1() {
        try {
            String className = "ProductItem";
            Class<?> root = Class.forName("com.luongtx.jsontojavaclass.pojo." + className);
            Field[] fields = root.getDeclaredFields();
            for (Field field : fields) {
                System.out.println(field.getName() + " : " + field.getType());
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    String json = "{\n" +
            "            \"customerInfo\": {\n" +
            "                \"adressData\": {\n" +
            "                    \"city\": \"City2\",\n" +
            "                    \"countryISO\": \"DE\",\n" +
            "                    \"street\": \"ABS Street\",\n" +
            "                    \"companyName\": \"ABC Compayy\",\n" +
            "                    \"plz\": \"22222\"\n" +
            "                },\n" +
            "                \"businessPartnerId\": {\n" +
            "                    \"identifierType\": \"GP\",\n" +
            "                    \"id\": \"1234\",\n" +
            "                    \"origin\": \"ZDF\"\n" +
            "                },\n" +
            "                \"firstContact\": {\n" +
            "                    \"company\": \"ABC Compayy\"\n" +
            "                }\n" +
            "            },\n" +
            "            \"contractInfo\": {\n" +
            "                \"contractProlongationPeriod\": {\n" +
            "                    \"value\": 1,\n" +
            "                    \"unit\": \"Month\"\n" +
            "                },\n" +
            "                \"contractEndDate\": \"2025-01-31T00:00:00.00000Z\",\n" +
            "                \"contractTerm\": {\n" +
            "                    \"value\": 36,\n" +
            "                    \"unit\": \"Month\"\n" +
            "                },\n" +
            "                \"contractType\": \"Contract_Amendment_TC\",\n" +
            "                \"contractCancelationPeriod\": {\n" +
            "                    \"value\": 3,\n" +
            "                    \"unit\": \"Month\"\n" +
            "                },\n" +
            "                \"contractStartDate\": \"2022-02-01T00:00:00.00000Z\"\n" +
            "            },\n" +
            "            \"headerInfo\": {\n" +
            "                \"documentsLanguage\": \"DE\",\n" +
            "                \"quoteOwner\": {\n" +
            "                    \"company\": \"Deutsche Telekom Business Solutions GmbH\",\n" +
            "                    \"eMail\": \"Ahmed.Iftikhar@t-systems.com\",\n" +
            "                    \"firstName\": \"A\",\n" +
            "                    \"externalId\": {\n" +
            "                        \"identifierType\": \"CIAMId\",\n" +
            "                        \"id\": \"aiftikh\",\n" +
            "                        \"origin\": \"CIAM\"\n" +
            "                    },\n" +
            "                    \"lastName\": \"I\"\n" +
            "                },\n" +
            "                \"transmissionDate\": \"2022-03-01T09:39:34.40100Z\",\n" +
            "                \"quoteNumber\": \"01880105\",\n" +
            "                \"companyCode\": \"1449\",\n" +
            "                \"accountManager\": {\n" +
            "                    \"lastName\": \"I\",\n" +
            "                    \"salutation\": \"Mr.\",\n" +
            "                    \"firstName\": \"A\",\n" +
            "                    \"eMail\": \"Ahmed.Iftikhar@t-systems.com\"\n" +
            "                },\n" +
            "                \"quoteName\": \"Test_Proposal_2022\",\n" +
            "                \"leadingLBU\": \"Deutsche Telekom Business Solutions GmbH\",\n" +
            "                \"project\": {\n" +
            "                    \"identifierType\": \"ZDFVorhabenId\",\n" +
            "                    \"id\": \"01880105\",\n" +
            "                    \"origin\": \"ZDF\"\n" +
            "                },\n" +
            "                \"quoteRevisionName\": \"MASTER\",\n" +
            "                \"quoteRevisionNr\": \"0\",\n" +
            "                \"quoteId\": \"18624\"\n" +
            "            }\n" +
            "        }";

    @Test
    void testTreeConverter() {
        JsonToClassGenerator.generateClasses("QuoteInfo", json);
        System.out.println(ClassToTreeConverter.convertToTree("QuoteInfo"));
    }

    public class NodeBean {
        private int id;
        private String name;

        public NodeBean() {
        }

        public NodeBean(int id, String name) {
            this.id = id;
            this.name = name;
        }

        // standard getters and setters
    }

    @Test
    void testJsonNode() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();

//        String json = "{ \"f1\":\"Hello\", \"f2\":null }";

        JsonNode jsonNode = objectMapper.readTree(json);

        String customerInfo = jsonNode.get("customerInfo").asText();
        System.out.println(customerInfo);
    }

    @Test
    void testJsonNode2() {
        ObjectMapper mapper = new ObjectMapper();
    }

}
