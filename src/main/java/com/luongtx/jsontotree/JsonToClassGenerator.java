package com.luongtx.jsontotree;

import com.sun.codemodel.JCodeModel;
import org.jsonschema2pojo.*;
import org.jsonschema2pojo.rules.RuleFactory;

import java.io.*;
import java.net.URL;

public class JsonToClassGenerator {

    public static void generateClasses(String rootClassName, String json) {
        String packageName = "com.luongtx.jsontojavaclass.pojo";
        String resourcePath = "src/main/resources";
        File jsonFile = new File(resourcePath + "/" + rootClassName + ".json");
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(jsonFile)))) {
            bw.write(json);
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String targetPath = "target";
        File outputPojoDirectory = new File(targetPath + "/" + "converted_pojo");
        outputPojoDirectory.mkdirs();
        try {
            generateClasses(jsonFile.toURI().toURL(), outputPojoDirectory, packageName, rootClassName);
        } catch (IOException e) {
            System.out.println("Encountered issue while converting to pojo: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static void generateClasses(URL inputJsonUrl, File outputJavaClassDirectory, String packageName, String javaClassName) throws IOException {
        JCodeModel jcodeModel = new JCodeModel();

        GenerationConfig config = new DefaultGenerationConfig() {
            @Override
            public boolean isGenerateBuilders() {
                return true;
            }

            @Override
            public SourceType getSourceType() {
                return SourceType.JSON;
            }

            @Override
            public boolean isIncludeAdditionalProperties() {
                return false;
            }
        };

        SchemaMapper mapper = new SchemaMapper(new RuleFactory(config, new Jackson2Annotator(config), new SchemaStore()), new SchemaGenerator());
        mapper.generate(jcodeModel, javaClassName, packageName, inputJsonUrl);

        jcodeModel.build(outputJavaClassDirectory);
    }

}
