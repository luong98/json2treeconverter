package com.luongtx.jsontotree;

import com.google.gson.Gson;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ClassToTreeConverter {
    public static class Entry {

        private String name;

        public Entry(String name) {
            this.name = name;
        }

        private List<Entry> children;

        public void add(Entry node) {
            if (children == null)
                children = new ArrayList<Entry>();
            children.add(node);
        }
    }

    public static void traverse(Class<?> rootClass, ClassToTreeConverter.Entry rootNode, Set<Class<?>> classes) {
        if (!classes.contains(rootClass)) {
            return;
        }
        Field[] children = rootClass.getDeclaredFields();
        for (Field child : children) {
            ClassToTreeConverter.Entry childNode = new ClassToTreeConverter.Entry(child.getName());
            rootNode.add(childNode);
            Class<?> childClass = child.getType();
            Type genericType = child.getGenericType();
            if (genericType instanceof ParameterizedType) {
                ParameterizedType pType = (ParameterizedType) genericType;
                childClass = (Class<?>) pType.getActualTypeArguments()[0];
            }
            traverse(childClass, childNode, classes);
        }

    }

    public static Set<Class<?>> findAllClassesInPackage(String packageName) {
        Reflections reflections = new Reflections(packageName, new SubTypesScanner(false));
        return new HashSet<>(reflections.getSubTypesOf(Object.class));
    }

    public static String convertToTree(String rootClassName) {
        String basePackage = "com.luongtx.jsontojavaclass.pojo";
        Set<Class<?>> classes = findAllClassesInPackage(basePackage);
        try {
            Class<?> rootClass = Class.forName(basePackage + "." + rootClassName);
            ClassToTreeConverter.Entry rootNode = new ClassToTreeConverter.Entry(rootClass.getSimpleName());
            traverse(rootClass, rootNode, classes);
            Gson g = new Gson();
            return g.toJson(rootNode);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

}
